let resumeData = {
    "imagebaseurl": "http://lucasrosinelliresume.netlify.com/",
    "firstName": "Lucas",
    "lastName": "Rosinelli da Rocha",
    "fullName": "Lucas Rosinelli da Rocha",
    "shortName": "Lucas Rosinelli",
    "role": "full stack developer",
    "roleDescription": "Experienced full stack developer with solutions created focused on company needs. A team-player member who always keeps in mind that excellent interpersonal relationship is important as a high-quality product delivered on time.",
    "contact": {
        "linkedIn": "https://br.linkedin.com/in/lucasrosinelli/en",
        "facebook": "https://www.facebook.com/lucas.rosinelli",
        "instagram": "https://www.instagram.com/lucasrosinelli",
        "bitbucket": "https://bitbucket.org/lucasrosinelli",
        "github": "https://github.com/lucasrosinelli",
        "phone": {
            "unformatted": "351965507579",
            "display": "+351 965 507 579"
        },
        "email": "contact@lucasrosinelli.com",
        "skype": "lucas.rosinelli",
        "currentLocation": "Lisbon, Portugal",
        "currentLocationCountry": "pt"
    },
    "resume": {
        "workExperiences": [
            {
                "company": "Vision-box", "position": "Software engineer",
                "country": "pt", "location": "Lisbon, Portugal",
                "from": "March 2019", "to": "Present",
                "description": "",
                "duties": [],
                "achievements": []
            },
            {
                "company": "NEXXYS", "position": "Full stack developer",
                "country": "br", "location": "São Paulo, SP, Brazil",
                "from": "September 2012", "to": "March 2019",
                "description": "",
                "duties": [],
                "achievements": []
            },
            {
                "company": "DIGISYSTEM", "position": "Software developer",
                "country": "br", "location": "São Paulo, SP, Brazil",
                "from": "October 2006", "to": "August 2012",
                "description": "",
                "duties": [],
                "achievements": []
            },
            {
                "company": "Politec", "position": "Programmer",
                "country": "br", "location": "Taquaritinga, SP, Brazil",
                "from": "September 2004", "to": "June 2006",
                "description": "",
                "duties": [],
                "achievements": []
            }
        ],
        "skills": [
            { "name": "C#", "size": 80, "nameClass": "csharp", "sizeClass": "eigthy" },
            { "name": "ASP.NET MVC (4, 5)", "size": 70, "nameClass": "aspnetmvc", "sizeClass": "seventy" },
            { "name": "ASP.NET Web API", "size": 70, "nameClass": "aspnetwebapi", "sizeClass": "seventy" },
            { "name": "ASP.NET SignalR", "size": 60, "nameClass": "aspnetsignalr", "sizeClass": "sixty" },
            { "name": "Windows Forms", "size": 80, "nameClass": "windowsforms", "sizeClass": "eigthy" },
            { "name": "WPF", "size": 50, "nameClass": "wpf", "sizeClass": "fifty" },
            { "name": "WCF", "size": 50, "nameClass": "wcf", "sizeClass": "fifty" },
            { "name": "REST/SOAP", "size": 60, "nameClass": "restsoap", "sizeClass": "sixty" },
            { "name": "Entity Framework", "size": 60, "nameClass": "entityframework", "sizeClass": "sixty" },
            { "name": "Dapper", "size": 40, "nameClass": "dapper", "sizeClass": "forty" },
            { "name": "JavaScript", "size": 80, "nameClass": "javascript", "sizeClass": "eigthy" },
            { "name": "jQuery", "size": 70, "nameClass": "jquery", "sizeClass": "seventy" },
            { "name": "Bootstrap", "size": 65, "nameClass": "bootstrap", "sizeClass": "sixtyfive" },
            { "name": "AngularJS", "size": 50, "nameClass": "angular", "sizeClass": "fifty" },
            { "name": "React", "size": 45, "nameClass": "react", "sizeClass": "fortyfive" },
            { "name": "HTML", "size": 75, "nameClass": "html", "sizeClass": "seventyfive" },
            { "name": "CSS", "size": 65, "nameClass": "css", "sizeClass": "sixtyfive" },
            { "name": "SQL Server", "size": 80, "nameClass": "sqlserver", "sizeClass": "eigthy" },
            { "name": "MySQL", "size": 50, "nameClass": "mysql", "sizeClass": "fifty" },
            { "name": "Oracle", "size": 40, "nameClass": "oracle", "sizeClass": "forty" },
            { "name": "MongoDB", "size": 60, "nameClass": "mongodb", "sizeClass": "sixty" },
            { "name": "Ionic", "size": 60, "nameClass": "ionic", "sizeClass": "sixty" },
            { "name": "React Native", "size": 30, "nameClass": "reactnative", "sizeClass": "thirty" },
            { "name": "TFS", "size": 70, "nameClass": "tfs", "sizeClass": "seventy" },
            { "name": "GIT", "size": 50, "nameClass": "git", "sizeClass": "fifty" }
        ]
    },
}
export default resumeData;